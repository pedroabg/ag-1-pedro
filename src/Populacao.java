import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class Populacao {
	




	
	
	List<Individuo> individuos ;
	
	int geracao;

	public Populacao() {
		this.geracao = 1;
		individuos = new ArrayList<Individuo>();
	}
	public Populacao(int geracao) {
		this.geracao = geracao;
		individuos = new ArrayList<Individuo>();
	}
	
	public void Incializar(int quantidade, int numGenes){
	 	individuos = new ArrayList<Individuo>();
		for (int i = 0; i < quantidade; i++) {
			individuos.add(Individuo.GerarIndividuo(numGenes,1));
			
		}
		
	}
	
	public void Avaliar(Solucao solucao){
		
		for (Individuo individuo : individuos) {
			individuo.checaFitness(solucao);
			if(individuo.getFitness() >= solucao.getFitnessAdequado()){
				solucao.setSolucaoAceita(true);
			}
		}
		Collections.sort(individuos);
		
	}
	
	public Populacao novaGeracao(int numGenes){
		Populacao novaGeracao = new Populacao(this.getGeracao());		
		cruzamento();
		mutacao();
		novaGeracao.setIndividuos(selecao(this, numGenes));	
		return novaGeracao;
	}
	
	private void cruzamento(){
		
	}
	
	private void mutacao(){
		
	}
	
	private List<Individuo> selecao(Populacao velha, int numGenes){
		List<Individuo> individuos = new ArrayList<Individuo>();
		
		for (int i = 0; i < velha.individuos.size(); i++) {
			if(i < velha.individuos.size() / 2)
				individuos.add(this.individuos.get(i));
			else 
				individuos.add(Individuo.GerarIndividuo(numGenes,this.getGeracao()));
			
		}		
		return individuos;	
		
	}
	
	
	
	public int getMelhorFitness(){
		
		return individuos.get(0).getFitness();
	}
	
		
	
	public int getGeracao() {
		return geracao;
	}
	
	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}

	public List<Individuo> getIndividuos() {
		return individuos;
	}

	public void setIndividuos(List<Individuo> individuos) {
		this.individuos = individuos;
	}

	@Override
	public String toString() {
		return "Populacao [individuos=\n" + individuos + "]";
	}
	
	
	
	

}
