import java.lang.reflect.Array;
import java.util.Arrays;


public class Individuo implements Comparable<Individuo> {




	int fitness;
	int[] genoma;
	int geracao;
	static MersenneTwisterFast prng = new MersenneTwisterFast();
	

	
	
	public int getFitness() {
		return fitness;
	}



	public void setFitness(int fitness) {
		this.fitness = fitness;
	}



	public int[] getGenoma() {
		return genoma;
	}



	public void setGenoma(int[] genoma) {
		this.genoma = genoma;
	}



	
	

	public Individuo() {
		super();
		fitness = -1;
		
	}
	
	public void checaFitness(Solucao solucao){
		int fitness = 0;
		for (int i = 0; i < solucao.getAlvo().length(); i++) {
			if( Character.getNumericValue(solucao.getAlvo().charAt(i))  == genoma[i]){
				fitness++;
			}
		}
		
		this.fitness = fitness;
	}



	static Individuo GerarIndividuo(int numGenes, int geracao){		
		
		Individuo individuo = new Individuo();
		individuo.genoma = new int[numGenes];
		individuo.setGeracao(geracao);
		
		for (int i = 0; i < numGenes; i++) {
			
			individuo.genoma[i] =  prng.nextInt(10); 
		}
		
		return individuo;
	}
	
	
	
	@Override
	public String toString() {
		return "Individuo [genoma="
				+ Arrays.toString(genoma) + " ; fitness = "+fitness+" ; gera��o: "+geracao+"]\n";
	}



	public int compareTo(Individuo o) {
		// TODO Auto-generated method stub
		
		return o.getFitness() - this.fitness;
	}



	public int getGeracao() {
		return geracao;
	}



	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}
	
	
	
	
}
